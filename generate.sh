#!/bin/sh

echo generating 7bit combinator dictionaries...

echo lower

./mp32.bin --output-file dicts/bf_l_1.dict ?l
./mp32.bin --output-file dicts/bf_l_2.dict ?l?l
./mp32.bin --output-file dicts/bf_l_3.dict ?l?l?l
./mp32.bin --output-file dicts/bf_l_4.dict ?l?l?l?l

echo digit

./mp32.bin --output-file dicts/bf_d_1.dict ?d
./mp32.bin --output-file dicts/bf_d_2.dict ?d?d
./mp32.bin --output-file dicts/bf_d_3.dict ?d?d?d
./mp32.bin --output-file dicts/bf_d_4.dict ?d?d?d?d

echo upper

./mp32.bin --output-file dicts/bf_u_1.dict ?u
./mp32.bin --output-file dicts/bf_u_2.dict ?u?u
./mp32.bin --output-file dicts/bf_u_3.dict ?u?u?u
./mp32.bin --output-file dicts/bf_u_4.dict ?u?u?u?u

echo symbol

./mp32.bin --output-file dicts/bf_s_1.dict ?s
./mp32.bin --output-file dicts/bf_s_2.dict ?s?s
./mp32.bin --output-file dicts/bf_s_3.dict ?s?s?s
./mp32.bin --output-file dicts/bf_s_4.dict ?s?s?s?s

echo lower digit

./mp32.bin --custom-charset1=?l?d --output-file dicts/bf_ld_1.dict ?1
./mp32.bin --custom-charset1=?l?d --output-file dicts/bf_ld_2.dict ?1?1
./mp32.bin --custom-charset1=?l?d --output-file dicts/bf_ld_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?d --output-file dicts/bf_ld_4.dict ?1?1?1?1

echo lower upper

./mp32.bin --custom-charset1=?l?u --output-file dicts/bf_lu_1.dict ?1
./mp32.bin --custom-charset1=?l?u --output-file dicts/bf_lu_2.dict ?1?1
./mp32.bin --custom-charset1=?l?u --output-file dicts/bf_lu_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?u --output-file dicts/bf_lu_4.dict ?1?1?1?1

echo lower symbol

./mp32.bin --custom-charset1=?l?s --output-file dicts/bf_ls_1.dict ?1
./mp32.bin --custom-charset1=?l?s --output-file dicts/bf_ls_2.dict ?1?1
./mp32.bin --custom-charset1=?l?s --output-file dicts/bf_ls_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?s --output-file dicts/bf_ls_4.dict ?1?1?1?1

echo digit upper

./mp32.bin --custom-charset1=?d?u --output-file dicts/bf_du_1.dict ?1
./mp32.bin --custom-charset1=?d?u --output-file dicts/bf_du_2.dict ?1?1
./mp32.bin --custom-charset1=?d?u --output-file dicts/bf_du_3.dict ?1?1?1
./mp32.bin --custom-charset1=?d?u --output-file dicts/bf_du_4.dict ?1?1?1?1

echo digit symbol

./mp32.bin --custom-charset1=?d?s --output-file dicts/bf_ds_1.dict ?1
./mp32.bin --custom-charset1=?d?s --output-file dicts/bf_ds_2.dict ?1?1
./mp32.bin --custom-charset1=?d?s --output-file dicts/bf_ds_3.dict ?1?1?1
./mp32.bin --custom-charset1=?d?s --output-file dicts/bf_ds_4.dict ?1?1?1?1

echo upper symbol

./mp32.bin --custom-charset1=?u?s --output-file dicts/bf_us_1.dict ?1
./mp32.bin --custom-charset1=?u?s --output-file dicts/bf_us_2.dict ?1?1
./mp32.bin --custom-charset1=?u?s --output-file dicts/bf_us_3.dict ?1?1?1
./mp32.bin --custom-charset1=?u?s --output-file dicts/bf_us_4.dict ?1?1?1?1

echo lower digit upper

./mp32.bin --custom-charset1=?l?d?u --output-file dicts/bf_ldu_1.dict ?1
./mp32.bin --custom-charset1=?l?d?u --output-file dicts/bf_ldu_2.dict ?1?1
./mp32.bin --custom-charset1=?l?d?u --output-file dicts/bf_ldu_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?d?u --output-file dicts/bf_ldu_4.dict ?1?1?1?1

echo lower digit symbol

./mp32.bin --custom-charset1=?l?d?s --output-file dicts/bf_lds_1.dict ?1
./mp32.bin --custom-charset1=?l?d?s --output-file dicts/bf_lds_2.dict ?1?1
./mp32.bin --custom-charset1=?l?d?s --output-file dicts/bf_lds_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?d?s --output-file dicts/bf_lds_4.dict ?1?1?1?1

echo lower upper symbol

./mp32.bin --custom-charset1=?l?u?s --output-file dicts/bf_lus_1.dict ?1
./mp32.bin --custom-charset1=?l?u?s --output-file dicts/bf_lus_2.dict ?1?1
./mp32.bin --custom-charset1=?l?u?s --output-file dicts/bf_lus_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?u?s --output-file dicts/bf_lus_4.dict ?1?1?1?1

echo digit upper symbol

./mp32.bin --custom-charset1=?d?u?s --output-file dicts/bf_dus_1.dict ?1
./mp32.bin --custom-charset1=?d?u?s --output-file dicts/bf_dus_2.dict ?1?1
./mp32.bin --custom-charset1=?d?u?s --output-file dicts/bf_dus_3.dict ?1?1?1
./mp32.bin --custom-charset1=?d?u?s --output-file dicts/bf_dus_4.dict ?1?1?1?1

echo lower digit upper symbol

./mp32.bin --custom-charset1=?l?d?u?s --output-file dicts/bf_ldus_1.dict ?1
./mp32.bin --custom-charset1=?l?d?u?s --output-file dicts/bf_ldus_2.dict ?1?1
./mp32.bin --custom-charset1=?l?d?u?s --output-file dicts/bf_ldus_3.dict ?1?1?1
./mp32.bin --custom-charset1=?l?d?u?s --output-file dicts/bf_ldus_4.dict ?1?1?1?1

echo hex lower

./mp32.bin --custom-charset1=?dabcdef --output-file dicts/bf_hl_1.dict ?1
./mp32.bin --custom-charset1=?dabcdef --output-file dicts/bf_hl_2.dict ?1?1

echo hex upper

./mp32.bin --custom-charset1=?dABCDEF --output-file dicts/bf_hu_1.dict ?1
./mp32.bin --custom-charset1=?dABCDEF --output-file dicts/bf_hu_2.dict ?1?1
